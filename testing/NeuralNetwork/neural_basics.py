## Imports
import math
import numpy as np
from timeit import default_timer as timer

    
## Functions
def sigmoid_func(input):
    "Calculates the sigmoid value of the input"
    
    return 1/(1+math.e**(-input))
    
    
## Classes
class Neuron:
    """
    A class representing a single neuron
    ...
    
    ==========
    Attributes
    ==========    
    weights : numpy.array 
        row vector containing the weights for every input
    offset : float
        offset in the preferred trans_func
    trans_func: function
        function which calculates the final output of the neuron
    
    =======
    Methods
    =======
    calc(inputs)
        Calculates the activation level of the neuron based on the given inputs
    """
    
    def __init__(self,weights,offset,trans_func):
        self.weights    = np.array(weights)
        self.offset     = offset
        self.trans_func = trans_func
        
    def __iter__(self):
        return self
    
    def calc(self,inputs):
        """
        Calculates the activation level of the neuron based on the given inputs
        ...
        Inputs should be the same length as the weights defined in the Neuron
        instance. The final output is calculated as the weighted summary of the 
        inputs, then applying the defined function in the trans_func attribute. 
        """
        
        if inputs.ndim != 1:
            raise TypeError('Inputs must be a row vector')
        if len(inputs) != len(self.weights):
            raise TypeError('Inputs and weights does not have the same size. ' +
                             'This neuron excepts {} input(s)'.format(len(self.weights)) )

        return self.trans_func(np.dot(inputs,self.weights)+self.offset)
        
    
class Layer:
    """
    A class containing every neuron at the same level
    ...
    
    =========
    Attriutes
    =========
    neurons : Neuron
        Array of neurons on the same level
    size : int
        Number of neurons on the level
    index : int
        Current iteration index
        
    =======
    Methods
    =======
    calc(inputs)
        Calculates the activation level of every neuron on the level
    """
    
    def __init__(self,neurons):
        self.neurons = neurons
        self.size    = len(neurons)
        self.index   = 0
        
    def __iter__(self):
        return self
        
    def __next__(self):
        if self.index >= self.size:
            self.index = 0
            raise StopIteration
        self.index = self.index+1
        return self.neurons[self.index-1]
    
    def calc(self,inputs):
        """
        Calculates the activation level of every neuron in the layer, and returns
        them as a vector.
        ...
        The function uses numpy.vectorize for faster calculation - but it is
        slower, than iterating through the array, so we used that instead.
        """
        
        def one_neuron(neur,neur_inputs):
            return neur.calc(neur_inputs)
        
        #v_one_neuron = np.vectorize(one_neuron, excluded=['neur_inputs'])
        
        #return v_one_neuron(neur=self.neurons,neur_inputs=inputs)
        return np.array([n.calc(inputs) for n in self.neurons])

class plainLayer:
    """
    Single layer with no Neuron class, only with weight matrix
    ---
    It is determined to cause memory error with large number of 'neurons'.
    About 60% percent faster, but cannot be bigger than 40000 neuron with 16GB
    of RAM.
    """
    
    def __init__(self,weights,offsets,trans_func):
        self.weights    = np.array(weights)
        self.offsets    = np.array(offsets)
        self.trans_func = trans_func
        
    def calc(self,inputs):
        
        v_trans_func = np.vectorize(self.trans_func)
        
        ret = self.trans_func(np.dot(inputs,self.weights)+self.offsets)
        return ret
class Network:
    """
    Complete structure of a neural network
    ...
    
    ==========
    Attributes
    ==========
    Layers : Layer
        Every layer which the network consists of
    size : int
        Number of layers
    index : int
        Current iteration index
    
    =======
    Methods
    =======
    calc(inputs)
        Calculates the whole network
    """
    
    def __init__(self,Layers):
        self.Layers = Layers
        self.size   = len(Layers)
        self.index   = 0
        
    def __iter__(self):
        return self
    
    def __next__(self):
        if self.index >= self.size:
            self.index = 0
            raise StopIteration
        self.index = self.index + 1
        return self.Layers[self.index-1]
    
    def calc(self,inputs):
        inputs = np.array(inputs)
        for layer in self.Layers:
            inputs = layer.calc(inputs)
        return inputs        
    
    def shownetwork(self):
        inputnum         = len(self.Layers[0].neurons[0].weights)
        outputnum        = len(self.Layers[-1].neurons)
        layerconf        = [len(l.neurons) for l in self.Layers]
        totalneuron      = sum(layerconf)+inputnum
        print("Network with", inputnum, "inputs and", outputnum, "outputs:\n", 
              "    layer configuration:    ", layerconf, "\n"
              "     total number of neurons: ", totalneuron)
    
    @classmethod
    def byDesc(cls,layer_list,base_weight=0.1,base_offset=-0.5,base_func=sigmoid_func):
        Layers = []
        for index,neurnum in enumerate(layer_list):
            
            if index == 0:
                prev_neurnum = neurnum
            
            weight = [base_weight for _ in range(layer_list[index-1]) ]
            neuron = Neuron(weight,base_offset,base_func)
            Layers.append(Layer([neuron for _ in range(prev_neurnum)]))
            
            prev_neurnum = neurnum
            
        return Network(Layers)


