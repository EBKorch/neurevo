import neural_basics as nb
import timeit

size = [1000,10]

base_weight = 0.1
base_offset = 0.1

w = [base_weight for _ in range(size[0])]
weights = [w for _ in range(size[0])]
w = None
offsets = [base_offset for _ in range(size[0])]

layers = [nb.plainLayer(weights,offsets,nb.sigmoid_func) for _ in range(size[1])]
weights = None
offsets = None

plainNet = nb.Network(layers)
layers = None
Net = nb.Network.byDesc([size[0] for _ in range(size[1])],base_weight,base_offset)
#Net.shownetwork()

input = [0.1 for _ in range(size[0])]

print(timeit.timeit('Net.calc(input)','from __main__ import Net, input',number=1))
print(timeit.timeit('plainNet.calc(input)','from __main__ import plainNet, input',number=1))

#out1 = plainNet.calc(input)
#out2 = Net.calc(input)
#print(max(out1-out2))
