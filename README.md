# NeurEvo

Optimizing neural network(s) with evolution algorithm. Testing on a simple entity, given by its rigid body dynamic system, with a single goal: eat as much as possible.

## Neural networks

For a general overview about neural networks head to [Wikipedia](https://en.wikipedia.org/wiki/Neural_network).

The project uses its own implementation of neural network. 

## Evolution algorithm

For a general overview, [Wikipedia](https://en.wikipedia.org/wiki/Evolutionary_algorithm) is a good starting point.

The aim of the project is to optimize neural networks with evolution algorithm. This kind of omptimization strategy has the advantage that not only the weights, but also the configuration of the network can be varied.

## Environment

The neural network and the optimization strategy is used to make simple "organisms" learn how to move. The goal is to collect as much "food" as possible, during a predefined time.

### The organism

A simple entity is given by its rigid body dynamic system. The organism can control the torques occuring at the joint points. Some joints are able to "grab" the ground. The organism should also be provided with information about its environment. The current input from the environment, the current torque at the joints, the angular velocity at the joints are the input of the neural network.

# Details

A more detailed documentation should appear anytime soon in the _doc_.

The _testing_ folder contains scripts with ongoing implementations.